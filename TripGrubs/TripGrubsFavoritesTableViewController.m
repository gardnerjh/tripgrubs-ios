//
//  TripGrubsFavoritesTableViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 12/4/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import "TripGrubsFavoritesTableViewController.h"
#import "TripGrubsLocationDetailTableViewController.h"

@interface TripGrubsFavoritesTableViewController ()

@property (nonatomic, strong) NSArray *favoriteLocations;

@end

@implementation TripGrubsFavoritesTableViewController

@synthesize favoriteLocations = _favoriteLocations;

- (void)setFavoriteLocations:(NSArray *)favoriteLocations {
    if (_favoriteLocations != favoriteLocations) {
        _favoriteLocations = favoriteLocations;
        [self.tableView reloadData];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    NSArray *favoriteLocationArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"favoriteLocations"];
    if (favoriteLocationArray != self.favoriteLocations) {
        self.favoriteLocations = favoriteLocationArray;
    }
    [super viewWillAppear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.favoriteLocations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FavoriteCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *locationDict = [self.favoriteLocations objectAtIndex:indexPath.row];
    cell.textLabel.text = [locationDict objectForKey:@"name"];
    cell.detailTextLabel.text = [locationDict objectForKey:@"address"];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowLocationDetail"]) {
        TripGrubsLocationDetailTableViewController *destVC = segue.destinationViewController;
        NSIndexPath *selectedIndexPath = [self.tableView indexPathForSelectedRow];
        NSDictionary *locationDictionary = [self.favoriteLocations objectAtIndex:selectedIndexPath.row];
        destVC.location = locationDictionary;
        destVC.isFavorite = YES;
    }
}

@end
