//
//  TripGrubsLocationDetailTableViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 11/27/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <AddressBook/AddressBook.h>
#import "TripGrubsLocationDetailTableViewController.h"
#import "TripGrubsWebViewController.h"

@interface TripGrubsLocationDetailTableViewController ()

@end

@implementation TripGrubsLocationDetailTableViewController

@synthesize location = _location;
@synthesize isFavorite = _isFavorite;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *shows = [self.location objectForKey:@"shows"];
    int showCount = [shows count];
    return 1 + showCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return @"Location";
    } else {
        return @"Show";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"DetailCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"name";
                cell.detailTextLabel.text = [self.location objectForKey:@"name"];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 1:
                cell.textLabel.text = @"address";
                cell.detailTextLabel.text = [self.location objectForKey:@"address"];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 2:
                cell.textLabel.text = @"source";
                
                if ([[self.location objectForKey:@"source"] isEqualToString:@"FoodNetwork"]) {
                    cell.detailTextLabel.text = @"Food Network";
                } else {
                    cell.detailTextLabel.text = [self.location objectForKey:@"source"];
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                break;
            default:
                break;
        }
    } else {
        int showIndex = indexPath.section - 1;
        NSDictionary *showDict = [(NSArray *)[self.location objectForKey:@"shows"] objectAtIndex:showIndex];
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"name";
                cell.detailTextLabel.text = [showDict objectForKey:@"name"];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 1:
                cell.textLabel.text = @"host";
                cell.detailTextLabel.text = [showDict objectForKey:@"host"];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            case 2:
                cell.textLabel.text = @"episode";
                cell.detailTextLabel.text = [showDict objectForKey:@"episodeName"];
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                break;
            default:
                break;
        }
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section > 0 || indexPath.row == 0 || indexPath.row == 1) {
        return indexPath;
    } else {
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section > 0) {
        [self performSegueWithIdentifier:@"ShowWebView" sender:indexPath];
    } else if (indexPath.row == 0) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        NSString *messageText;
        if (self.isFavorite) {
            messageText = @"Remove from favorites?";
        } else {
            messageText = @"Add to favorites?";
        }
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"TripGrubs"
                      message:messageText
                     delegate:self
            cancelButtonTitle:@"Cancel"
            otherButtonTitles:@"OK", nil];
        
        [message show];
    } else if (indexPath.row == 1) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self launchAppleMapsDirections];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowWebView"]) {
        TripGrubsWebViewController *destVC = segue.destinationViewController;
        NSIndexPath *indexPath = (NSIndexPath *) sender;
        
        NSURL *url = [[NSURL alloc] init];
        NSString *urlString;
        
        if (indexPath.section == 0) {
            switch (indexPath.row) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
            }
        } else {
            int showIndex = indexPath.section - 1;
            NSDictionary *showDict = [(NSArray *)[self.location objectForKey:@"shows"] objectAtIndex:showIndex];
            switch (indexPath.row) {
                case 0:
                    urlString = [showDict objectForKey:@"siteUrl"];
                    destVC.title = [showDict objectForKey:@"name"];
                    break;
                case 1:
                    urlString = [showDict objectForKey:@"hostSiteUrl"];
                    destVC.title = [showDict objectForKey:@"host"];
                    break;
                case 2:
                    urlString = [showDict objectForKey:@"episodeSiteUrl"];
                    destVC.title = [showDict objectForKey:@"episodeName"];
                    break;
            }
        }
        
        if (urlString) {
            url = [[NSURL alloc] initWithString:urlString];
        }
        
        destVC.destinationUrl = url;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"OK"] && self.isFavorite) {
        [self removeFromFavorites];
    } else if ([title isEqualToString:@"OK"] && !self.isFavorite) {
        [self addToFavorites];
    }
}

- (void)removeFromFavorites {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *favoritesArray = [defaults objectForKey:@"favoriteLocations"];
    [favoritesArray removeObject:self.location];
    
    if ([defaults objectForKey:@"favoriteLocations"]) {
        [defaults removeObjectForKey:@"favoriteLocations"];
    }
    [defaults setObject:[[NSArray alloc] initWithArray:favoritesArray] forKey:@"favoriteLocations"];
    self.isFavorite = NO;
}

- (void)addToFavorites {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSMutableArray *favoritesArray = [[defaults objectForKey:@"favoriteLocations"] mutableCopy];
    if (!favoritesArray) {
        favoritesArray = [[NSMutableArray alloc] init];
    }
    [favoritesArray insertObject:self.location atIndex:0];
    
    if ([defaults objectForKey:@"favoriteLocations"]) {
        [defaults removeObjectForKey:@"favoriteLocations"];
    }
    [defaults setObject:[[NSArray alloc] initWithArray:favoritesArray] forKey:@"favoriteLocations"];
    self.isFavorite = YES;
}

- (void)launchAppleMapsDirections {
    NSArray *coordinates = [self.location objectForKey:@"geolocation"];
    CLLocationDegrees latitude = [[coordinates objectAtIndex:0] doubleValue];
    CLLocationDegrees longitude = [[coordinates objectAtIndex:1] doubleValue];
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    NSDictionary *addressDict = @{
        (NSString *) kABPersonAddressStreetKey : [self.location objectForKey:@"address"],
        (NSString *) kABPersonAddressStreetKey : @"",
        (NSString *) kABPersonAddressCityKey : @"",
        (NSString *) kABPersonAddressStateKey : @"",
        (NSString *) kABPersonAddressZIPKey : @"",
        (NSString *) kABPersonAddressCountryKey : @""
    };
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate addressDictionary:addressDict];
    
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = [self.location objectForKey:@"name"];
    
    [mapItem openInMapsWithLaunchOptions:[NSDictionary dictionaryWithObjectsAndKeys:MKLaunchOptionsDirectionsModeDriving, MKLaunchOptionsDirectionsModeKey, nil]];
}

@end
