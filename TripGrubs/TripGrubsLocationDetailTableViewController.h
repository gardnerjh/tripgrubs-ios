//
//  TripGrubsLocationDetailTableViewController.h
//  TripGrubs
//
//  Created by Jeff Gardner on 11/27/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripGrubsLocationDetailTableViewController : UITableViewController<UIAlertViewDelegate>

@property (nonatomic, strong) NSDictionary *location;
@property BOOL isFavorite;

@end
