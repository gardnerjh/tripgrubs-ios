//
//  TripGrubsLocationListTableViewController.h
//  TripGrubs
//
//  Created by Jeff Gardner on 11/22/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripGrubsLocationListTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *locationArray;

@end
