//
//  TripGrubsFavoritesTableViewController.h
//  TripGrubs
//
//  Created by Jeff Gardner on 12/4/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripGrubsFavoritesTableViewController : UITableViewController

@end
