//
//  TripGrubsWebViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 12/4/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import "TripGrubsWebViewController.h"

@interface TripGrubsWebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation TripGrubsWebViewController

@synthesize destinationUrl = _destinationUrl;
@synthesize webView = _webView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSURLRequest *request = [NSURLRequest requestWithURL:self.destinationUrl];
    [self.webView loadRequest:request];
    self.webView.delegate = self;
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView
{
    CGSize contentSize = theWebView.scrollView.contentSize;
    CGSize viewSize = self.view.bounds.size;
    
    float rw = viewSize.width / contentSize.width;
    
    theWebView.scrollView.minimumZoomScale = rw;
    theWebView.scrollView.maximumZoomScale = rw;
    theWebView.scrollView.zoomScale = rw;
}

@end
