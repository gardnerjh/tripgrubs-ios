//
//  TripGrubsSearchViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 10/28/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import "TripGrubsSearchViewController.h"
#import "TripGrubsRouteMapViewController.h"

@interface TripGrubsSearchViewController ()
@property (weak, nonatomic) IBOutlet UITextField *originTextField;
@property (weak, nonatomic) IBOutlet UITextField *destinationTextField;
@property (weak, nonatomic) IBOutlet UITextField *locationDistanceTextField;
@property (weak, nonatomic) IBOutlet UIStepper *locationDistanceStepper;
@end

@implementation TripGrubsSearchViewController

@synthesize originTextField = _originTextField;
@synthesize destinationTextField = _destinationTextField;
@synthesize locationDistanceTextField = _locationDistanceTextField;
@synthesize locationDistanceStepper = _locationDistanceStepper;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self prepopulateFields];
}

- (void)prepopulateFields {
    self.originTextField.text = @"5805 Dimes Rd, Derwood, MD 20855";
    self.destinationTextField.text = @"98 W 1230 N, Provo, UT 84604";
}

- (IBAction)locationDistanceStepperChanged {
    self.locationDistanceTextField.text = [NSString stringWithFormat:@"%g", self.locationDistanceStepper.value];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowLocationMap"]) {
        TripGrubsRouteMapViewController *destVC = segue.destinationViewController;
        
        destVC.originAddress = self.originTextField.text;
        destVC.destinationAddress = self.destinationTextField.text;
        destVC.locationDistance = [NSNumber numberWithDouble:[self.locationDistanceTextField.text doubleValue]];
    }
}

- (IBAction)resignActive {
    [self.originTextField resignFirstResponder];
    [self.destinationTextField resignFirstResponder];
}

@end
