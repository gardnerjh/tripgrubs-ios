//
//  main.m
//  TripGrubs
//
//  Created by Jeff Gardner on 10/28/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TripGrubsAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TripGrubsAppDelegate class]));
    }
}
