//
//  TripGrubsRouteMapViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 10/31/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "TripGrubsRouteMapViewController.h"
#import "AFHTTPClient.h"
#import "AFJSONRequestOperation.h"
#import "TripGrubsMapAnnotation.h"
#import "TripGrubsLocationListTableViewController.h"
#import "TripGrubsLocationDetailTableViewController.h"

@interface TripGrubsRouteMapViewController ()
@property (nonatomic, strong) NSDictionary *originCoordinates;
@property (nonatomic, strong) NSDictionary *destinationCoordinates;
@property (nonatomic, strong) NSMutableArray *path;
@property (nonatomic, strong) NSArray *locationArray;
@end

@implementation TripGrubsRouteMapViewController

@synthesize mapView = _mapView;
@synthesize originAddress = _originAddress;
@synthesize destinationAddress = _destinationAddress;
@synthesize locationDistance = _locationDistance;
@synthesize path = _path;
@synthesize locationArray = _locationArray;

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay {
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor blueColor];
    polylineView.lineWidth = 20.0;
    polylineView.alpha = 0.5;
    
    return polylineView;
}

- (MKAnnotationView *)mapView:(MKMapView *)map viewForAnnotation:(id<MKAnnotation>)annotation
{
    TripGrubsMapAnnotation *mapAnnotation = (TripGrubsMapAnnotation *) annotation;
    MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
    annotationView.pinColor = [mapAnnotation getAnnotationColor];
    annotationView.canShowCallout = YES;
    
    // Add button to view
    if ((![mapAnnotation.identifier isEqualToString:@"origin"]) && (![mapAnnotation.identifier isEqualToString:@"destination"])) {
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self action:@selector(didSelectLocationCallout:) forControlEvents:UIControlEventTouchUpInside];
        rightButton.tag = [mapAnnotation.identifier intValue];
        annotationView.rightCalloutAccessoryView = rightButton;
    }
    
    return annotationView;
}

- (void)didSelectLocationCallout:(id) sender {
    [self performSegueWithIdentifier:@"ShowLocationDetail" sender:sender];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.mapView setDelegate:self];
    [self initMap];
}

- (void)initMap {
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?sensor=true&origin=%@&destination=%@", self.originAddress, self.destinationAddress];
    NSString *escapedUrlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:escapedUrlString];
    NSURLRequest *request  = [NSURLRequest requestWithURL:url];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSInteger statusCode = response.statusCode;
        if (statusCode == 200) {
            [self parseRouteResponse:JSON];
            [self addRouteAnnotations];
            [self retrieveLocations];
        } else {
            NSLog(@"Request unsuccessful - status code: %d", response.statusCode);
            //TODO: handle error somehow
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError* error, id JSON) {
        NSLog(@"Error sending request: %@", error);
    }];
    [operation start];
}

- (void)parseRouteResponse:(NSDictionary *)response {
    NSArray *routes = [response objectForKey:@"routes"];
    NSDictionary *route = [routes lastObject];
    if (route) {
        NSString *overviewPolyline = [[route objectForKey: @"overview_polyline"] objectForKey:@"points"];
        self.path = [self decodePolyLine:overviewPolyline];
        
        NSDictionary *firstLeg = [[route objectForKey:@"legs"] firstObject];
        self.originAddress = [firstLeg objectForKey:@"start_address"];
        self.originCoordinates = [firstLeg objectForKey:@"start_location"];
        
        NSDictionary *lastLeg = [[route objectForKey:@"legs"] lastObject];
        self.destinationAddress = [lastLeg objectForKey:@"end_address"];
        self.destinationCoordinates = [lastLeg objectForKey:@"end_location"];
        
        [self addMapViewOverlay];
    }
}

-(NSMutableArray *)decodePolyLine:(NSString *)encodedStr {
    NSMutableString *encoded = [[NSMutableString alloc] initWithCapacity:[encodedStr length]];
    [encoded appendString:encodedStr];
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:location];
    }
    
    return array;
}

- (void)addMapViewOverlay {
    NSInteger numberOfSteps = self.path.count;
    
    CLLocationCoordinate2D coordinates[numberOfSteps];
    for (NSInteger index = 0; index < numberOfSteps; index++) {
        CLLocation *location = [self.path objectAtIndex:index];
        CLLocationCoordinate2D coordinate = location.coordinate;
        
        coordinates[index] = coordinate;
    }
    
    MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:coordinates count:numberOfSteps];
    [self.mapView addOverlay:polyLine];
    [self.mapView setVisibleMapRect:polyLine.boundingMapRect];
}

- (void)addRouteAnnotations {
    NSString *originTitle = @"Origin";
    NSString *originSubtitle = self.originAddress;
    [self addMapViewAnnotation:@"endPoint" identifier:@"origin" title:originTitle subtitle:originSubtitle withCoordinates:self.originCoordinates];
    
    NSString *destinationTitle = @"Destination";
    NSString *destinationSubtitle = self.destinationAddress;
    [self addMapViewAnnotation:@"endPoint" identifier:@"destination" title:destinationTitle subtitle:destinationSubtitle withCoordinates:self.destinationCoordinates];
}

- (void)addMapViewAnnotation:(NSString *)type identifier:(NSString *)identifier title:(NSString *)title subtitle:(NSString *)subtitle withCoordinates:(NSDictionary *)coordinates {
    TripGrubsMapAnnotation *annotation = [[TripGrubsMapAnnotation alloc] init];
    annotation.locationType = type;
    annotation.coordinate = CLLocationCoordinate2DMake([[coordinates objectForKey:@"lat"] doubleValue], [[coordinates objectForKey:@"lng"] doubleValue]);
    annotation.identifier = identifier;
    annotation.title = title;
    annotation.subtitle = subtitle;
    [self.mapView addAnnotation:annotation];
}

- (void)retrieveLocations {
    NSMutableArray *coordinateArray = [[NSMutableArray alloc] init];
    for (NSInteger index = 0; index < self.path.count; index++) {
        CLLocation *location = [self.path objectAtIndex:index];
        NSMutableDictionary *locationCoordinates = [[NSMutableDictionary alloc] init];
        [locationCoordinates setObject:[NSNumber numberWithDouble:location.coordinate.latitude] forKey:@"latitude"];
        [locationCoordinates setObject:[NSNumber numberWithDouble:location.coordinate.longitude] forKey:@"longitude"];
        [coordinateArray addObject:locationCoordinates];
    }

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:[coordinateArray mutableCopy] forKey:@"coordinates"];
    [parameters setObject:self.locationDistance forKey:@"distance"];
    
    NSURL *url = [NSURL URLWithString:@"http://api.tripgrubs.com"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient setParameterEncoding:AFJSONParameterEncoding];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST"
                                                            path:@"locations/route"
                                                      parameters:[[NSDictionary alloc] initWithDictionary:parameters]];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSInteger statusCode = response.statusCode;
        if (statusCode == 200) {
            //NSLog(@"%@", JSON);
            NSDictionary *locations = [[NSDictionary alloc] initWithDictionary:JSON];
            NSMutableArray *locationArray = [[NSMutableArray alloc] init];
            
            for(NSString *key in locations) {
                NSDictionary *location = [[NSDictionary alloc] initWithDictionary:[locations objectForKey:key]];
                NSMutableDictionary *coordinates = [[NSMutableDictionary alloc] init];
                [coordinates setObject:[location objectForKey:@"geolocation"][0] forKey:@"lat"];
                [coordinates setObject:[location objectForKey:@"geolocation"][1] forKey:@"lng"];
                
                [self addMapViewAnnotation:@"location" identifier:[NSString stringWithFormat:@"%d", locationArray.count] title:[location objectForKey:@"name"] subtitle:[location objectForKey:@"address"] withCoordinates:coordinates];
                
                [locationArray addObject:location];
            }
            self.locationArray = [[NSArray alloc] initWithArray:locationArray];
        } else {
            NSLog(@"Request unsuccessful - status code: %d", response.statusCode);
            //TODO: handle error somehow
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError* error, id JSON) {
        NSLog(@"Error sending request: %@", error);
    }];
    [operation start];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowLocationList"]) {
        TripGrubsLocationListTableViewController *destVC = segue.destinationViewController;
        destVC.locationArray = self.locationArray;
    } else if ([segue.identifier isEqualToString:@"ShowLocationDetail"]) {
        UIButton *button = sender;
        TripGrubsLocationDetailTableViewController *destVC = segue.destinationViewController;
        
        NSDictionary *location = [self.locationArray objectAtIndex:button.tag];
        destVC.location = location;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *favoriteLocations = [defaults objectForKey:@"favoriteLocations"];
        if ([favoriteLocations containsObject:location]) {
            destVC.isFavorite = YES;
        } else {
            destVC.isFavorite = NO;
        }
    }
}

@end
