//
//  TripGrubsMapAnnotation.m
//  TripGrubs
//
//  Created by Jeff Gardner on 11/21/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import "TripGrubsMapAnnotation.h"

@implementation TripGrubsMapAnnotation

@synthesize locationType = _locationType;
@synthesize identifier = _identifier;

- (MKPinAnnotationColor)getAnnotationColor {
    if ([self.locationType isEqualToString:@"endPoint"]) {
        return MKPinAnnotationColorRed;
    } else if ([self.locationType isEqualToString:@"location"]) {
        return MKPinAnnotationColorGreen;
    } else {
        return MKPinAnnotationColorPurple;
    }
}

@end
