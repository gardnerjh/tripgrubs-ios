//
//  TripGrubsLocationListTableViewController.m
//  TripGrubs
//
//  Created by Jeff Gardner on 11/22/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import "TripGrubsLocationListTableViewController.h"
#import "TripGrubsLocationDetailTableViewController.h"

@interface TripGrubsLocationListTableViewController ()
@end

@implementation TripGrubsLocationListTableViewController

@synthesize locationArray = _locationArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.locationArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"LocationCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }
    
    NSDictionary *locationDict = [self.locationArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [locationDict objectForKey:@"name"];
    cell.detailTextLabel.text = [locationDict objectForKey:@"address"];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowLocationDetail"]) {
        TripGrubsLocationDetailTableViewController *destVC = segue.destinationViewController;
        int selectedRowNum = [self.tableView indexPathForSelectedRow].row;
        NSDictionary *locationDictionary = [self.locationArray objectAtIndex:selectedRowNum];
        destVC.location = locationDictionary;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *favoriteLocations = [defaults objectForKey:@"favoriteLocations"];
        if ([favoriteLocations containsObject:locationDictionary]) {
            destVC.isFavorite = YES;
        } else {
            destVC.isFavorite = NO;
        }
    }
}

@end
