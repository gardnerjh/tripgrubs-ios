//
//  TripGrubsMapAnnotation.h
//  TripGrubs
//
//  Created by Jeff Gardner on 11/21/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface TripGrubsMapAnnotation : MKPointAnnotation

@property (nonatomic, strong) NSString *locationType;

- (MKPinAnnotationColor)getAnnotationColor;
@property (nonatomic, strong) NSString *identifier;

@end