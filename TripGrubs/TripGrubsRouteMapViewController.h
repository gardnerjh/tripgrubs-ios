//
//  TripGrubsRouteMapViewController.h
//  TripGrubs
//
//  Created by Jeff Gardner on 10/31/12.
//  Copyright (c) 2012 JeffyG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TripGrubsRouteMapViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSString *originAddress;
@property (strong, nonatomic) NSString *destinationAddress;
@property (strong, nonatomic) NSNumber *locationDistance;

@end
